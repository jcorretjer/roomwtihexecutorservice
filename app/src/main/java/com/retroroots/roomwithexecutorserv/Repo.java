package com.retroroots.roomwithexecutorserv;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class Repo
{
    private D d;

    @Inject
    public Repo(D d)
    {
        this.d = d;
    }

    public void Insert(Ent ent)
    {
        new InsertService(new Runnable()
        {
            @Override
            public void run()
            {
                d.Insert(ent);
            }
        }).run();
    }

    private class InsertService implements Runnable
    {
        private ExecutorService service;

        private Runnable runnable;

        public InsertService(Runnable runnable)
        {
            service = Executors.newFixedThreadPool(1);

            this.runnable = runnable;
        }

        @Override
        public void run()
        {
            Future<?> future = service.submit(runnable);

            while (true)
            {
                if(future.isDone())
                    break;
            }

            TerminateExecutorService(service);
        }
    }

    private void TerminateExecutorService(ExecutorService executorService)
    {
        //Stop taking tasks
        executorService.shutdown();

        try
        {
            //Try to terminate all tasks after 800 milli
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS))
                executorService.shutdownNow();
        }

        catch (InterruptedException e)
        {
            //Forcefully terminate any remaining tasks
            executorService.shutdownNow();
        }
    }
}
