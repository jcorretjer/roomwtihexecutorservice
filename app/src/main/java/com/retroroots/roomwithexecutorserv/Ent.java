package com.retroroots.roomwithexecutorserv;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Ent
{
    private String name;

    @PrimaryKey(autoGenerate = true)
    private long id;

    public Ent(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }
}
