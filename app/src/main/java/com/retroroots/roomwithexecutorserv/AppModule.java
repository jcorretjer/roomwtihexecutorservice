package com.retroroots.roomwithexecutorserv;

import android.app.Application;

import androidx.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class AppModule
{
    @Provides
    @Singleton
    public DB providesUrlCacheDatabase(Application application)
    {
        return Room.databaseBuilder(application, DB.class, "DB")
                   .fallbackToDestructiveMigration() //We need this or else the app will crash when the db scheme changes
                   .build();
    }

    @Provides
    public D providesUrlCacheDAO(DB urlCacheDB)
    {
        return urlCacheDB.D();
    }
}
