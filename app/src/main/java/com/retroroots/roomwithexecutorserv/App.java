package com.retroroots.roomwithexecutorserv;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class App extends Application
{
}
