package com.retroroots.roomwithexecutorserv;

import androidx.room.Dao;
import androidx.room.Insert;

@Dao
public interface D
{
    @Insert
    public void Insert(Ent ent);
}
